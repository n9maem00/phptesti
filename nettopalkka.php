<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nettopalkka</title>
</head>
<body>
<?php
    $brutto= filter_input(INPUT_POST,'brutto', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $ennakko= filter_input(INPUT_POST,'ennakko', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $elake= filter_input(INPUT_POST,'elake', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $vakuutus= filter_input(INPUT_POST,'vakuutus', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $netto = ($brutto - ($brutto * ($ennakko / 100)) - ($brutto * ($elake / 100)) - ($brutto * ($vakuutus / 100)));
    printf("<p>Nettopalkka on %0.02f €</p>",$netto);
    ?>
    <a href="index.html">Laske uudestaan.</a>
</body>
</html>